package services;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.net.InetAddress;

/**
 * This class does the necessary checks for internet connectivity.
 *
 * @author Okan Kaya
 */

@SuppressWarnings("ConstantConditions")
public class WifiChecks {

	private final Context context;

	public WifiChecks(Context context) {
		this.context = context;
	}


	/**
	 * This method checks if phone is connected to internet through Wifi or Mobile Provider.
	 *
	 * @return true or false depending of connectivity.
	 */

	public boolean isWifiConnected() {

		try {
			ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo networkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
			if (networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
				return true;
			}
		} catch (Exception e) {
			Log.d("", "WIFI ERROR" + e.getMessage());
		}
		return false;
	}

	/**
	 * This method checks if phone is connected to internet at all.
	 *
	 * @return true or false depending on connection.
	 */

	public boolean checkInternetConnection() {
        /*try {
            InetAddress ipAddr = InetAddress.getByName("google.com");
            //You can replace it with your name
            return !ipAddr.equals("");

        } catch (Exception e) {
            return false;
        }*/

		try {
			ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

			NetworkInfo networkInfo = cm.getActiveNetworkInfo();
			return networkInfo != null && networkInfo.isConnectedOrConnecting();
		} catch (Exception e) {
			Log.d("", "checkInternetConnection" + e.getMessage());
		}
		return false;
	}
}
