# WallpaperChanger

An Android application to change wallpaper of your phone automatically. 
Wallpapers are got from various Reddit subreddits. 
 I used Glide to process images and Gson to get image links from Reddit.

![](https://i.imgur.com/nm43hum.png "Main Screen")  ![](https://i.imgur.com/UyRR321.png "Setting Screen")
