package services;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import model.ImageLink;

/**
 * @author OKAN KAYA created on 13.8.2017.
 *         This class gets image links from reddit Json by using Gson.
 *         It works on its own thread so it does not slow down the UI.
 */


public class GetUrlsAsyncTask extends AsyncTask<Void, Void, ArrayList<String>> {


    /**
     * Allowed image extensions that are going to be fetched.
     */
    private final Set<String> ALLOWED_IMAGE_ENDINGS = new HashSet<>(Arrays.asList("jpg", "gif", "tiff", "png"));
    private final Context context;
    private final DatabaseHandler databaseHandler;
    private String jsonUrl;
    private ProgressDialog progressDialog;

    public GetUrlsAsyncTask(Context context) {
        this.context = context;


        databaseHandler = DatabaseHandler.getDbInstance(context);
        WifiChecks wifiChecks = new WifiChecks(context);

        if (!wifiChecks.checkInternetConnection()) {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

    }


    /**
     * @return the link that is used.
     */
    private String getJsonUrl() {
        return jsonUrl;
    }

    /**
     * Method that sets the sub links that is going to be used.
     *
     * @param jsonUrl sub link
     */
    public void setJsonUrl(String jsonUrl) {
        this.jsonUrl = jsonUrl;
    }


    /**
     * method to show waiting dialog.
     */
    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();


    }

    /**
     * @param url is checked if the url parameter is an image or not
     * @return true or false depending on the ending of link
     */

    private boolean isAllowedImageEnding(String url) {
        if (!url.contains(".")) {
            return false;
        }
        String[] split = url.split("\\.");
        return ALLOWED_IMAGE_ENDINGS.contains(split[split.length - 1].toLowerCase());
    }

    /**
     * This method get image links outside of main thread so it does not effect performance of the app.
     * @return it returns a list of image links.
     */


    /**
     * Async method that does image link fetching process on background.
     *
     * @param
     * @return
     */
    @Override
    protected ArrayList<String> doInBackground(Void... params) {
        ArrayList<String> imageUrlList = new ArrayList<>();
        Gson gson = new Gson();

        try {
            URL url = new URL(jsonUrl);
            HttpURLConnection urlConnection;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.addRequestProperty("User-Agent", "Mozilla 5/0");

            JsonObject object;
            object = gson.fromJson(new InputStreamReader(urlConnection.getInputStream()), JsonObject.class);

            for (JsonElement element : object.getAsJsonObject("data").getAsJsonArray("children")) {
                JsonObject data = element.getAsJsonObject().getAsJsonObject("data");

                String imageUrl = data.getAsJsonPrimitive("url").getAsString();

                if (isAllowedImageEnding(imageUrl)) {
                    imageUrlList.add(imageUrl);
                }
            }
        } catch (Exception e) {
            Log.e("asyncError", e.toString());
        }
        return imageUrlList;
    }


    /**
     * In this method, if the previously given url to doInBackground method is compared
     * to the subreddit url variables, if they matched a new object is created and added to DB with
     * its own source.
     *
     * @param result We get the returned list into result list.
     */

    @Override
    protected void onPostExecute(ArrayList<String> result) {
        super.onPostExecute(result);

        if (progressDialog.isShowing())
            progressDialog.dismiss();
        for (Source source : Source.values()) {
            if (this.getJsonUrl().equals(source.getUrl())) {
                onPostExecuteHelper(source.getSource(), result);
            }
        }
    }

    /**
     * This method prevents repetion from happening.
     *
     * @param source that is going to be used.
     * @param result list that is going to be used.
     */
    private void onPostExecuteHelper(String source, ArrayList<String> result) {
        for (String link : result) {
            databaseHandler.addImage(new ImageLink(link, source));
        }
    }
}