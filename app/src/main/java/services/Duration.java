package services;

import android.os.Build;
import android.util.Log;

import java.util.Arrays;


/**
 * Enum class is used to prevent repetitive if else statements from happening.
 * Variable duration is for the duration of periodic image change.
 * Variable label is for Spinner that is used by user to choose duration.
 */

public enum Duration {
    NOT_ACTIVE(0, "not active"),
    MIN15(900000, "15 min"),
    MIN30(1800000, "30 min"),
    HOUR(3600000, "Hour"),
    HOURS6(21600000, "6 hours"),
    HOUR12(43200000, "12 hours"),
    DAY(86400000, "Day"),
    DAY3(259200000, "3 days"),
    WEEK(604800000, "Week");

    private final long duration;
    private final String label;

    Duration(long duration, String label) {
        this.duration = duration;
        this.label = label;
    }

    /**
     * @param label is used to return the duration that is connected to that label
     * @return duration of the specified label
     * If the device has an API level that higher than 19, the stream will be used
     * otherwise it won't be
     */
    public static long getDurationByLabel(String label) {

        //checks if API level is smaller than 20
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Arrays.stream(values())
                    .filter(e -> e.getLabel().equals(label))
                    .findFirst()
                    .orElse(NOT_ACTIVE)
                    .getDuration();
        }
        Log.d("LOW API LEVEL", "getDurationByLabel: ");

        long ms = 0;
        for (Duration d : Duration.values()) {
            if (d.getLabel().equals(label)) {
                ms = d.getDuration();
            }
        }
        return ms;
    }

    private long getDuration() {
        return duration;
    }

    private String getLabel() {
        return label;
    }
}