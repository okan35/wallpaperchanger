package services;

import android.app.IntentService;
import android.content.Intent;

/**
 * Created by pc on 18.11.2017.
 *
 * @author Okan Kaya
 *         This class works as a service for changing images periodically.
 */

public class PeriodicImageChangeService extends IntentService {

    private final SetWallpaper setWallpaper = new SetWallpaper(this);

    public PeriodicImageChangeService() {
        super("service");

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        setWallpaper.setWallpaperPeriodically();

    }
}
