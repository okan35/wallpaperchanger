package model;

public class ImageLink {

    private String imageLink;
    private String imageSource;

    public ImageLink() {

    }

    public ImageLink(String imageLink, String imageSource) {
        this.imageLink = imageLink;
        this.imageSource = imageSource;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
