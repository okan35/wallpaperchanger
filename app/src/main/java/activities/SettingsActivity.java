package activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.Toast;

import com.okan.kaya.wallpperchanger.R;

import services.AlarmReceiver;
import services.DatabaseHandler;
import services.Duration;
import services.GetUrlsAsyncTask;
import services.Source;
import services.WifiChecks;

public class SettingsActivity extends AppCompatActivity {

    private final WifiChecks wifiChecks = new WifiChecks(SettingsActivity.this);
    private Switch switchEarthPorn;
    private Switch switchWallpapers;
    private Switch switchWallpaper;
    private Switch switchSkyPorn;
    private Switch switchAstroPhoto;
    private Switch switchOnlyWifi;
    private Switch switchAbandonedPorn;
    private Switch switchMilitaryPorn;
    private Switch switchTookPicture;
    private Switch switchEyeBleach;
    private Switch switchNaturePics;
    private Switch switchPic;
    private Switch switchWinterPorn;
    private Switch switchSpringPorn;
    private Switch switchGamerPorn;
    private Switch switchSummerPorn;
    private Switch switchAutumnPorn;
    private Spinner spinnerInterval;
    private final DatabaseHandler databaseHandler = DatabaseHandler.getDbInstance(this);
    public static boolean isWifiOn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_layout);

        spinnerInterval = (Spinner) findViewById(R.id.timer_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.hours, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerInterval.setAdapter(adapter);


        switchOnlyWifi = (Switch) findViewById(R.id.switch_only_wifi);
        switchEarthPorn = (Switch) findViewById(R.id.switch_earth_porn);
        switchWallpapers = (Switch) findViewById(R.id.switch_wallpapers);
        switchWallpaper = (Switch) findViewById(R.id.switch_wallpaper);
        switchSkyPorn = (Switch) findViewById(R.id.switch_skyporn);
        switchAstroPhoto = (Switch) findViewById(R.id.switch_astrophotography);
        switchAbandonedPorn = (Switch) findViewById(R.id.switch_abandoned);
        switchMilitaryPorn = (Switch) findViewById(R.id.switch_military);
        switchEyeBleach = (Switch) findViewById(R.id.switch_eyebleach);
        switchTookPicture = (Switch) findViewById(R.id.switch_tookpicture);
        switchNaturePics = (Switch) findViewById(R.id.switch_nature_pics);
        switchWinterPorn = (Switch) findViewById(R.id.switch_winter_porn);
        switchSpringPorn = (Switch) findViewById(R.id.switch_spring_porn);
        switchSummerPorn = (Switch) findViewById(R.id.switch_summer);
        switchAutumnPorn = (Switch) findViewById(R.id.switch_autumn);
        switchGamerPorn = (Switch) findViewById(R.id.switch_gamer);
        switchPic = (Switch) findViewById(R.id.switch_pic);

        if (savedInstanceState != null) {
            spinnerInterval.setSelection(savedInstanceState.getInt("spinnerInterval", 0));
        }

        spinnerInterval.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = parent.getSelectedItem().toString();


                long millisecond = Duration.getDurationByLabel(text);

                if (millisecond != 0) {
                    scheduleAlarm(millisecond);
                } else {
                    cancelSchedule();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        switchOnlyWifi.setOnClickListener(v -> {
            if (switchOnlyWifi.isChecked()) {
                isWifiOn = true;
                Log.d("SwitchOnlyWifi", "onClick: only wifi is ON ");
            } else {
                isWifiOn = false;
                Log.d("SwitchOnlyWifi", "onClick: only wifi is OFF ");
            }

        });


        switchEarthPorn.setOnClickListener(v -> switchHelper(switchEarthPorn, Source.EARTHPORN.getUrl(), Source.EARTHPORN.getSource()));

        switchWinterPorn.setOnClickListener(v -> switchHelper(switchWinterPorn, Source.WINTERPORN.getUrl(), Source.WINTERPORN.getSource()));

        switchSpringPorn.setOnClickListener(v -> switchHelper(switchSpringPorn, Source.SPRINGPORN.getUrl(), Source.SPRINGPORN.getSource()));

        switchSkyPorn.setOnClickListener(v -> switchHelper(switchSkyPorn, Source.SKYPORN.getUrl(), Source.SKYPORN.getSource()));

        switchAstroPhoto.setOnClickListener(v -> switchHelper(switchAstroPhoto, Source.ASTROPHOTO.getUrl(), Source.ASTROPHOTO.getSource()));

        switchWallpaper.setOnClickListener(v -> switchHelper(switchWallpaper, Source.WALLPAPER.getUrl(), Source.WALLPAPER.getSource()));

        switchWallpapers.setOnClickListener(v -> switchHelper(switchWallpapers, Source.WALLPAPERS.getUrl(), Source.WALLPAPERS.getSource()));

        switchAbandonedPorn.setOnClickListener(v -> switchHelper(switchAbandonedPorn, Source.ABANDONEDPORN.getUrl(), Source.ABANDONEDPORN.getSource()));

        switchMilitaryPorn.setOnClickListener(v -> switchHelper(switchMilitaryPorn, Source.MILITARYPORN.getUrl(), Source.MILITARYPORN.getSource()));

        switchPic.setOnClickListener(v -> switchHelper(switchPic, Source.PIC.getUrl(), Source.PIC.getSource()));

        switchEyeBleach.setOnClickListener(v -> switchHelper(switchEyeBleach, Source.EYEBLEACH.getUrl(), Source.EYEBLEACH.getSource()));

        switchTookPicture.setOnClickListener(v -> switchHelper(switchTookPicture, Source.TOOKPICTURE.getUrl(), Source.TOOKPICTURE.getSource()));

        switchNaturePics.setOnClickListener(v -> switchHelper(switchNaturePics, Source.NATUREPICS.getUrl(), Source.NATUREPICS.getSource()));

        switchGamerPorn.setOnClickListener(v -> switchHelper(switchGamerPorn, Source.GAMERPORN.getUrl(), Source.GAMERPORN.getSource()));

        switchAutumnPorn.setOnClickListener(v -> switchHelper(switchAutumnPorn, Source.AUTUMNPORN.getUrl(), Source.AUTUMNPORN.getSource()));

        switchSummerPorn.setOnClickListener(v -> switchHelper(switchSummerPorn, Source.SUMMERPORN.getUrl(), Source.SUMMERPORN.getSource()));
    }


    /**
     * Method that processes the given time interval -in milliseconds- to change image periodically
     *
     * @param millis is for the time interval that is chosen by user
     */

    private void scheduleAlarm(Long millis) {
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);

        final PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        long firstMillis = System.currentTimeMillis();

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Log.d("SCHEDULE", "scheduleAlarm: " + millis + "");
        if (alarmManager != null) {
            alarmManager.setInexactRepeating(AlarmManager.RTC_WAKEUP, firstMillis, millis, pendingIntent);
        }
    }

    /**
     * This method cancels the current periodic image change service.
     */

    private void cancelSchedule() {
        Log.d("SCHEDULE", "CANCELLED");
        Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        if (alarm != null) {
            alarm.cancel(pendingIntent);
        }
    }


    /**
     * Method for preventing code from repeating.
     *
     * @param jsonUrl is for setting url of the sub that is going to be used for images.
     */
    private void setJsonUrlOnAsyncTask(String jsonUrl) {

        GetUrlsAsyncTask getUrlsAsyncTask = new GetUrlsAsyncTask(SettingsActivity.this);
        getUrlsAsyncTask.setJsonUrl(jsonUrl);
        getUrlsAsyncTask.execute();

    }

    /**
     * Helper method for Wifi actions.
     *
     * @return condition of wifi.
     */
    private boolean isOnlyWifiNotConnected() {
        return switchOnlyWifi.isChecked() && !wifiChecks.isWifiConnected();
    }


    /**
     * A method for preventing repetive code from happening.
     *
     * @param subSwitch The switch that is going to be used.
     * @param subUrl    The url that is going to be used for getting image links.
     * @param subSource The source that is going to be used in DB adding/deleting.
     */

    private void switchHelper(Switch subSwitch, String subUrl, String subSource) {

        if (isOnlyWifiNotConnected()) {
            Toast.makeText(SettingsActivity.this, "You are not connected to Wifi", Toast.LENGTH_SHORT).show();
            return;
        }
        if (subSwitch.isChecked()) {
            setJsonUrlOnAsyncTask(subUrl);
        } else {
            databaseHandler.deleteLinks(subSource);
        }

    }


    /**
     * Saves state of the app onPause
     */
    @Override
    public void onPause() {
        super.onPause();

        final SharedPreferences sharedPref = getSharedPreferences("SettingsActivity.java", 0);
        final SharedPreferences.Editor prefEditor = sharedPref.edit();

        String spinnerValue = spinnerInterval.getSelectedItem().toString();
        prefEditor.putString("item", spinnerValue);
        prefEditor.putBoolean("switchSkyPorn", switchSkyPorn.isChecked());
        prefEditor.putBoolean("switchEarthPorn", switchEarthPorn.isChecked());
        prefEditor.putBoolean("switchEyeBleach", switchEyeBleach.isChecked());
        prefEditor.putBoolean("switchMilitaryPorn", switchMilitaryPorn.isChecked());
        prefEditor.putBoolean("switchPic", switchPic.isChecked());
        prefEditor.putBoolean("switchTookPicture", switchTookPicture.isChecked());
        prefEditor.putBoolean("switchAbandonedPorn", switchAbandonedPorn.isChecked());
        prefEditor.putBoolean("switchWallpaper", switchWallpaper.isChecked());
        prefEditor.putBoolean("switchWallpapers", switchWallpapers.isChecked());
        prefEditor.putBoolean("switchAstroPhoto", switchAstroPhoto.isChecked());
        prefEditor.putBoolean("switchOnlyWifi", switchOnlyWifi.isChecked());
        prefEditor.putBoolean("switchAutumnPorn", switchAutumnPorn.isChecked());
        prefEditor.putBoolean("switchSummerPorn", switchSummerPorn.isChecked());
        prefEditor.putBoolean("switchGamerPorn", switchGamerPorn.isChecked());
        prefEditor.putBoolean("switchWinterPorn", switchWinterPorn.isChecked());
        prefEditor.putBoolean("switchNaturePics", switchNaturePics.isChecked());
        prefEditor.putBoolean("switchSpringPorn", switchSpringPorn.isChecked());

        prefEditor.apply();
    }

    /**
     * Saves state of the app onResume
     */
    @Override
    public void onResume() {
        super.onResume();
        final SharedPreferences sharedPref = getSharedPreferences("SettingsActivity.java", 0);


        String spinnerValue = sharedPref.getString("item", "nothing");
        ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) spinnerInterval.getAdapter();

        int spinnerPosition = arrayAdapter.getPosition(spinnerValue);

        spinnerInterval.setSelection(spinnerPosition);
        switchEarthPorn.setChecked(sharedPref.getBoolean("switchEarthPorn", false));
        switchEyeBleach.setChecked(sharedPref.getBoolean("switchEyeBleach", false));
        switchWallpaper.setChecked(sharedPref.getBoolean("switchWallpaper", false));
        switchSkyPorn.setChecked(sharedPref.getBoolean("switchSkyPorn", false));
        switchWallpapers.setChecked(sharedPref.getBoolean("switchWallpapers", false));
        switchAstroPhoto.setChecked(sharedPref.getBoolean("switchAstroPhoto", false));
        switchOnlyWifi.setChecked(sharedPref.getBoolean("switchOnlyWifi", false));
        switchAbandonedPorn.setChecked(sharedPref.getBoolean("switchAbandonedPorn", false));
        switchTookPicture.setChecked(sharedPref.getBoolean("switchTookPicture", false));
        switchMilitaryPorn.setChecked(sharedPref.getBoolean("switchMilitaryPorn", false));
        switchPic.setChecked(sharedPref.getBoolean("switchPic", false));
        switchSummerPorn.setChecked(sharedPref.getBoolean("switchSummerPorn", false));
        switchAutumnPorn.setChecked(sharedPref.getBoolean("switchAutumnPorn", false));
        switchGamerPorn.setChecked(sharedPref.getBoolean("switchGamerPorn", false));
        switchWinterPorn.setChecked(sharedPref.getBoolean("switchWinterPorn", false));
        switchNaturePics.setChecked(sharedPref.getBoolean("switchNaturePics", false));
        switchSpringPorn.setChecked(sharedPref.getBoolean("switchSpringPorn", false));

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("spinnerInterval", spinnerInterval.getSelectedItemPosition());
    }


}