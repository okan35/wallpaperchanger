package services;

/**
 * This Enum class is heavily used by onPostExecute of Asynctask
 * and also used by SettingsActivity to prevent code repetition from happening.
 */

public enum Source {

    EARTHPORN("https://www.reddit.com/r/earthporn.json", "earthporn"),
    WALLPAPERS("https://www.reddit.com/r/wallpapers.json", "wallpapers"),
    WALLPAPER("https://www.reddit.com/r/wallpaper.json", "wallpaper"),
    SKYPORN("https://www.reddit.com/r/skyporn.json", "skyporn"),
    ASTROPHOTO("https://www.reddit.com/r/astrophotography.json", "astrophoto"),
    ABANDONEDPORN("https://www.reddit.com/r/abandonedporn.json", "abandonedporn"),
    MILITARYPORN("https://www.reddit.com/r/militaryporn.json", "militaryporn"),
    TOOKPICTURE("https://www.reddit.com/r/itookapicture.json", "tookpicture"),
    NATUREPICS("https://www.reddit.com/r/naturepics.json", "naturepics"),
    WINTERPORN("https://www.reddit.com/r/winterporn.json", "winterporn"),
    SPRINGPORN("https://www.reddit.com/r/springporn.json", "springporn"),
    GAMERPORN("https://www.reddit.com/r/gamerporn.json", "gamerporn"),
    SUMMERPORN("https://www.reddit.com/r/summerporn.json", "summerporn"),
    PIC("https://www.reddit.com/r/pic.json", "pic"),
    EYEBLEACH("https://www.reddit.com/r/eyebleach.json", "eyebleach"),
    AUTUMNPORN("https://www.reddit.com/r/autumnporn.json", "autumnporn");

    private final String url;
    private final String source;

    Source(String url, String source) {
        this.url = url;
        this.source = source;
    }

    public String getSource() {
        return this.source;
    }

    public String getUrl() {
        return this.url;
    }
}