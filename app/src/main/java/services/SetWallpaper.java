package services;

import android.app.WallpaperManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import activities.SettingsActivity;
import model.ImageLink;

public class SetWallpaper {
    private final Context context;
    private final Random random = new Random();
    private final DatabaseHandler databaseHandler;
    private final WifiChecks wifiChecks;

    public SetWallpaper(Context context) {
        this.context = context;
        databaseHandler = DatabaseHandler.getDbInstance(context);
        wifiChecks = new WifiChecks(context);
    }

    /**
     * This method prevents TransactionTooLargeException from happening as the bitmaps are quite large
     * so we make their size smaller.
     *
     * @param bm Bitmap that is going to be used.
     * @return bitmap that got smaller.
     */

    private Bitmap getResizedBitmap(Bitmap bm) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) 1200) / width;
        float scaleHeight = ((float) 1200) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        // RESIZE THE BIT MAP
        matrix.postScale(scaleWidth, scaleHeight);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(
                bm, 0, 0, width, height, matrix, false);
        bm.recycle();
        return resizedBitmap;
    }

    /**
     * This method is used in preview activity to change image when user clicks refresh.
     *
     * @param imageView is the view that is going to be used where the method is.
     */

    public void setWallpaper(final ImageView imageView) {
        if (!dBandConnectionChecks()) {
            return;
        }

        Glide.with(context).load(getRandomImageLink()).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);
                Bitmap newBitmap;
                try {
                    newBitmap = getResizedBitmap(resource);
                    imageView.setImageBitmap(newBitmap);
                    wallpaperManager.setBitmap(newBitmap);
                    Toast.makeText(context, "Wallpaper Changed", Toast.LENGTH_SHORT).show();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });

    }

    /**
     * This method is mainly used for periodic image change service.
     */

    public void setWallpaperPeriodically() {
        if (!dBandConnectionChecks()) {
            return;
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        try {
            Bitmap bitmap;
            WallpaperManager wallpaperManager = WallpaperManager.getInstance(context);

            bitmap = Glide.with(context).load(getRandomImageLink()).asBitmap().into(width, height).get();
            Bitmap newBitmap = getResizedBitmap(bitmap);

            wallpaperManager.setBitmap(newBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to get a random image link as string
     *
     * @return returns a random image link as string
     */
    private String getRandomImageLink() {
        List<ImageLink> imageLinks = databaseHandler.getAllImageLinks();
        int randomNumber = random.nextInt(imageLinks.size());
        return imageLinks.get(randomNumber).getImageLink();
    }


    /**
     * Method that does all the necessary checks.
     *
     * @return true if check results are true
     */
    private boolean dBandConnectionChecks() {
        if (!wifiChecks.checkInternetConnection()) {
            Toast.makeText(context, "No Internet Connection", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (SettingsActivity.isWifiOn && !wifiChecks.isWifiConnected()) {
            Toast.makeText(context, "Wifi is not connected", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (databaseHandler.getImageLinksCount() <= 0) {
            Toast.makeText(context, "Please choose at least one source for images", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
