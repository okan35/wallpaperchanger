package services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import model.ImageLink;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "imageManager";
    private static final String TABLE_IMAGES = "images";
    private static final String KEY_ID = "KEY ID";
    private static final String KEY_LINK = "imageLink";
    private static final String IMG_SOURCE = "imageSource";
    private static DatabaseHandler dbInstance = null;

    private DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DatabaseHandler getDbInstance(Context context) {
        if (dbInstance == null) {
            dbInstance = new DatabaseHandler(context);

        }

        return dbInstance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String createDB = "CREATE TABLE " + TABLE_IMAGES + "( " +
                KEY_ID + " INTEGER PRIMARY KEY, " +
                KEY_LINK + " TEXT NOT NULL, " +
                IMG_SOURCE + " TEXT NOT NULL" +
                ")";
        db.execSQL(createDB);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_IMAGES);
        // Create tables again
        onCreate(db);
    }

    public void addImage(ImageLink imageLink) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(KEY_LINK, imageLink.getImageLink());
        contentValues.put(IMG_SOURCE, imageLink.getImageSource());

        sqLiteDatabase.insert(TABLE_IMAGES, null, contentValues);

        sqLiteDatabase.close();
    }

    public void deleteLinks(String source) {
        SQLiteDatabase sql = this.getWritableDatabase();
        sql.delete(TABLE_IMAGES, IMG_SOURCE + "= ?", new String[]{source});
    }

    @SuppressWarnings("unused")
    public void deleteAllRows() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        String q = "delete from " + TABLE_IMAGES;
        sqLiteDatabase.execSQL(q);
    }

    public List<ImageLink> getAllImageLinks() {
        List<ImageLink> imageLinkList = new ArrayList<>();

        String query = "SELECT * FROM " + TABLE_IMAGES;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                ImageLink imageLink = new ImageLink();
                imageLink.setImageSource(cursor.getString(0));
                imageLink.setImageLink(cursor.getString(1));
                imageLinkList.add(imageLink);

            } while (cursor.moveToNext());
        }
        cursor.close();
        return imageLinkList;
    }

    public int getImageLinksCount() {
        String query = "SELECT COUNT(*) FROM " + TABLE_IMAGES;
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        try (Cursor cursor = sqLiteDatabase.rawQuery(query, null)) {
            cursor.moveToFirst();

            return cursor.getInt(0);
        }

    }
}