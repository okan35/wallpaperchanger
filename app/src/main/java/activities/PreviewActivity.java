package activities;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.ImageView;

import com.okan.kaya.wallpperchanger.R;

import services.SetWallpaper;


/**
 * This activity shows the images that is got from the reddit source
 */


public class PreviewActivity extends AppCompatActivity {

    private ImageView previewImageView;
    private final SetWallpaper setWallpaper = new SetWallpaper(PreviewActivity.this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_preview_layout);

        Button settingsButton = (Button) findViewById(R.id.button_settings);
        Button refreshButton = (Button) findViewById(R.id.button_refresh_preview);
        previewImageView = (ImageView) findViewById(R.id.preview_image_view);

        //sets wallpaper to imageview in the beginning of app
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
        Drawable drawable = wallpaperManager.getDrawable();
        Bitmap newBitmap = drawableToBitmap(drawable);
        previewImageView.setImageBitmap(newBitmap);

        settingsButton.setOnClickListener(v -> {
            Intent i = new Intent(PreviewActivity.this, SettingsActivity.class);
            startActivity(i);
        });

        refreshButton.setOnClickListener(v -> setWallpaper.setWallpaper(previewImageView));
    }


    /**
     * @param drawable is for drawable which will be converted to bitmap.
     * @return bitmap that is going to be used for setting wallpaper.
     */

    private Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    public void onResume() {
        super.onResume();
        WallpaperManager wallpaperManager = WallpaperManager.getInstance(this);
        Drawable drawable = wallpaperManager.getDrawable();
        Bitmap newBitmap = drawableToBitmap(drawable);
        previewImageView.setImageBitmap(newBitmap);
    }
}
